(function ($) { // this closure helps us keep our variables to ourselves.
 
    var url = AJS.contextPath() + "/rest/xproduct-admin/1.0/";
 
    $(document).ready(function() {
        $.ajax({
            url: url,
            dataType: "json"
        }).done(function(config) { // when the configuration is returned...
        	$("#name").val(config.name);
        	$("#age").val(config.age);
            AJS.$("#admin").submit(function(e) {
                updateConfig();
            });
        });
    });
    
    function updateConfig() {
  	  AJS.$.ajax({
  	    url: url,
  	    type: "POST",
  	    contentType: "application/json",
  	    data: '{ "name": "' + AJS.$("#name").attr("value") + '", "age": ' +  AJS.$("#age").attr("value") + ' }',
  	    processData: false
  	  });
  	}

})(AJS.$ || jQuery);

