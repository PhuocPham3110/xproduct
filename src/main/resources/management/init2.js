(function ($) {
    var url = AJS.contextPath() + "/rest/xproduct-admin/1.0/";
 
    $(document).ready(function() {
        $.ajax({
        	type: 'GET',
        	url: url,
            dataType: "json"
        }).done(function(config) {
        	if (config.name != null) {
	        	document.getElementById("name").innerHTML=config.name;
	        	document.getElementById("age").innerHTML=config.age;
	        	$("#btnEdit").show();
	        	document.getElementById('btnCreate').disabled = true;
	        	document.getElementById('btnDelete').disabled = false;
        	} else {
        		document.getElementById('btnDelete').disabled = true;
        	}
        });
    });
    
})(AJS.$ || jQuery);

function deleteConfig() {
	  AJS.$.ajax({
	    url: AJS.contextPath() + "/rest/xproduct-admin/1.0/",
	    type: "DELETE",
	    dataType: "json",
	    contentType: "application/json",
	    processData: false
	  }).done(function() {
		  location.href= AJS.contextPath() + "/secure/viewAll.jspa";
	  });
}

function getView() {
	location.href= AJS.contextPath() + "/secure/viewAll.jspa";
}
