require([
    "jquery",
    "jira/beta/AppView"
], function (
    $,
    AppView
    ){
    $(function() {
        // Kick things off by creating the **App**.
        new AppView();
    });
});
