define("jira/beta/TodoView",
    [
        "backbone",
        "jquery",
        "underscore"
    ], function (
        Backbone,
        $,
        _
    ) {
        // Todo Item View
        // --------------

        // The DOM element for a todo item...
        var TodoView = Backbone.View.extend({

            //... is a list tag.
            tagName: 'li',

            // Cache the template function for a single item.
            template: _.template( $('#item-template').html() ),

            // The DOM events specific to an item.
            events: {
                'dblclick label': 'edit',
                'keypress .edit': 'updateOnEnter',
                'blur .edit': 'close',
                'click .destroy': 'clear',
                'click .toggle': 'toggleCompleted'
            },

            // The TodoView listens for changes to its model, re-rendering. Since there's
            // a one-to-one correspondence between a **Todo** and a **TodoView** in this
            // app, we set a direct reference on the model for convenience.
            initialize: function() {
                this.listenTo(this.model, 'change', this.render);
                this.listenTo(this.model, 'destroy', this.remove);
            },

            // Re-renders the titles of the todo item.
            render: function() {
                this.$el.html( this.template( this.model.attributes ) );
                this.$input = $('.edit');
                return this;
            },

            // Switch this view into `"editing"` mode, displaying the input field.
            edit: function() {
                this.$el.addClass('editing');
                this.$input.focus();
            },

            clear: function () {
                this.model.destroy({
                    success: function() {
                        console.log("delete success");
                    },
                    error: function() {
                        console.log("delete fail");
                    }
                });
            },

            // Close the `"editing"` mode, saving changes to the todo.
            close: function() {
                var value = this.$input.val().trim();

                if ( value ) {
                    this.model.save({ title: value });
                }

                this.$el.removeClass('editing');
            },

            // If you hit `enter`, we're through editing the item.
            updateOnEnter: function( e ) {
                var ENTER_KEY = 13;
                if ( e.which === ENTER_KEY ) {
                    this.close();
                }
            },

            toggleCompleted: function() {
                this.model.toggle();
            }
        });
        return TodoView;
    });