define("jira/beta/BaseModel",
    [
        "backbone"
    ], function (
        Backbone
    ) {
        var BaseModel = Backbone.Model.extend({
            initialize: function(options){
                this.name = options.name;
                alert("Hello guy " + this.name); // used for backbone model, view, collection
            }
        });
        return BaseModel;
    });