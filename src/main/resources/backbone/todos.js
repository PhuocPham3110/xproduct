define("jira/beta/TodoList",
    [
        "backbone",
        "jira/beta/TodoModel"
    ], function (
        Backbone,
        TodoModel
    ) {
        var TodoList = Backbone.Collection.extend({

            // Reference to this collection's model.
            model: TodoModel,

            // Save all of the todo items under the `"todos-backbone"` namespace.
            localStorage: new Backbone.LocalStorage('todos-backbone'),

            // Filter down the list of all todo items that are finished.
            completed: function () {
                return this.filter(function (todo) {
                    return todo.get('completed');
                });
            },

            // Filter down the list to only todo items that are still not finished.
            remaining: function () {
                return this.without.apply(this, this.completed());
            },
        });
        return new TodoList();
    });