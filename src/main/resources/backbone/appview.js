define("jira/beta/AppView",
    [
        "backbone",
        "jquery",
        "underscore",
        "jira/beta/TodoList",
        "jira/beta/TodoView"
    ], function (
        Backbone,
        $,
        _,
        TodoList,
        TodoView
    ) {
        // The Application
        // ---------------

        // Our overall **AppView** is the top-level piece of UI.
        var AppView = Backbone.View.extend({

            // Instead of generating a new element, bind to the existing skeleton of
            // the App already present in the HTML.
            el: '.todoapp',

            // Our template for the line of statistics at the bottom of the app.
            statsTemplate: _.template( $('#stats-template').html() ),

            // New
            // Delegated events for creating new items, and clearing completed ones.
            events: {
                'keypress .new-todo': 'createOnEnter',
                'click .clear-completed': 'clearCompleted',
                'click .toggle-all': 'toggleAllComplete',
                'click #completed': 'completedItems',
                'click #all': 'allItems',
                'click #active': 'activeItems',
            },

            // At initialization we bind to the relevant events on the `Todos`
            // collection, when items are added or changed. Kick things off by
            // loading any preexisting todos that might be saved in *localStorage*.
            initialize: function() {
                console.log("inside initialize");
                this.allCheckbox = $('.toggle-all')[0];
                this.$input = $('.new-todo');
                this.$footer = $('.footer');
                this.$main = $('.main');
                this.listenTo(TodoList, 'add', this.addOne);
                this.listenTo(TodoList, 'reset', this.addAll);

                this.listenTo(TodoList, 'all', this.render);
                TodoList.fetch();
            },

            // New
            // Re-rendering the App just means refreshing the statistics -- the rest
            // of the app doesn't change.
            render: function() {
                var completed = TodoList.completed().length;
                var remaining = TodoList.remaining().length;

                if ( TodoList.length ) {
                    this.$main.show();
                    this.$footer.show();

                    this.$footer.html(this.statsTemplate({
                        completed: completed,
                        remaining: remaining
                    }));

                    //this.$('#filters li a')
                    //    .removeClass('selected')
                    //    .filter('[href="#/' + ( app.TodoFilter || '' ) + '"]')
                    //    .addClass('selected');
                } else {
                    this.$main.hide();
                    this.$footer.hide();
                }

                this.allCheckbox.checked = !remaining;
            },

            // Add a single todo item to the list by creating a view for it, and
            // appending its element to the `<ul>`.
            addOne: function( todo ) {
                var view = new TodoView({ model: todo });
                $('.todo-list').append(view.render().el );
            },

            // Add all items in the **Todos** collection at once.
            addAll: function() {
                this.$('.todo-list').html('');
                TodoList.each(this.addOne, this);
            },

            // New
            // Generate the attributes for a new Todo item.
            newAttributes: function() {
                return {
                    title: this.$input.val().trim(),
                    completed: false
                };
            },

            // New
            // If you hit return in the main input field, create new Todo model,
            // persisting it to localStorage.
            createOnEnter: function( event ) {
                var ENTER_KEY = 13;
                if ( event.which !== ENTER_KEY || !this.$input.val().trim() ) {
                    return;
                }

                TodoList.create( this.newAttributes() );
                this.$input.val('');
            },

            // New
            // Clear all completed todo items, destroying their models.
            clearCompleted: function() {
                _.invoke(TodoList.completed(), 'destroy');
                return false;
            },

            // New
            toggleAllComplete: function() {
                var completed = this.allCheckbox.checked;

                TodoList.each(function( todo ) {
                    todo.save({
                        'completed': completed
                    });
                });
            },

            completedItems: function () {
                TodoList.reset(TodoList.completed());
                $(".filters li a #completed").addClass("selected");
                $("a").removeClass("selected");
            },

            allItems: function () {
                console.log(TodoList.length);
                TodoList.reset(TodoList);
                $(".filters li a #all").addClass("selected");
                $("a").removeClass("selected");
            },

            activeItems: function () {
                TodoList.reset(TodoList.remaining());
                $(".filters li a #active").addClass("selected");
                $("a").removeClass("selected");
            },
        });
        return AppView;
    });