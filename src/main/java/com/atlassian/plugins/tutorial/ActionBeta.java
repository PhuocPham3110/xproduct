package com.atlassian.plugins.tutorial;

import java.util.Map;

import org.apache.commons.collections.MapUtils;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.web.action.ActionViewDataMappings;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class ActionBeta extends JiraWebActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5650466895867705874L;
	
	@Override
	protected String doExecute() throws Exception {
		return SUCCESS;
	}
	
}
