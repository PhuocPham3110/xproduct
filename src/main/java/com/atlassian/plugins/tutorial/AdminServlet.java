package com.atlassian.plugins.tutorial;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.tutorial.ConfigResource.Config;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;

public class AdminServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	public static final String PLUGIN_STORAGE_KEY = Config.class.getName();
	@ComponentImport
	private final UserManager userManager;
	@ComponentImport
	private final LoginUriProvider loginUriProvider;
	@ComponentImport
	private final TemplateRenderer renderer;
	@ComponentImport
	private final PluginSettingsFactory pluginSettingsFactory;
//	@ComponentImport
//	private PageBuilderService pageBuilderService;

	public AdminServlet(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer renderer, PluginSettingsFactory pluginSettingsFactory) {
		this.userManager = userManager;
		this.loginUriProvider = loginUriProvider;
		this.renderer = renderer;
		this.pluginSettingsFactory = pluginSettingsFactory;
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String username = userManager.getRemoteUsername(request);
	    if (username == null || !userManager.isSystemAdmin(username))
	    {
	      redirectToLogin(request, response);
	      return;
	    }
	    response.setContentType("text/html;charset=utf-8");
	    renderer.render("admin.vm", response.getWriter());
	}
	
//	@Override
//	protected void doPost(HttpServletRequest req, HttpServletResponse response)
//		throws ServletException, IOException {
//	PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
//	pluginSettings.put(PLUGIN_STORAGE_KEY + ".name", req.getParameter("name"));
//	pluginSettings.put(PLUGIN_STORAGE_KEY + ".age", req.getParameter("age"));
//	response.sendRedirect("http://localhost:2990/jira/rest/xproduct-admin/1.0/");
//	}
	
	private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
	  response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
	}
	 
	private URI getUri(HttpServletRequest request)
	{
	  StringBuffer builder = request.getRequestURL();
	  if (request.getQueryString() != null)
	  {
	    builder.append("?");
	    builder.append(request.getQueryString());
	  }
	  return URI.create(builder.toString());
	}
}