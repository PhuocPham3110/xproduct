package com.atlassian.plugins.tutorial;

import java.util.Enumeration;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.action.ActionViewDataMappings;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugins.tutorial.ConfigResource.Config;

public class ActionAlpha extends JiraWebActionSupport {

	private static final long serialVersionUID = 9061991190352054091L;
	private static final Logger LOGGER = LoggerFactory.getLogger(ActionAlpha.class);
	private String name = "a default value";
	private String age = "24";

	public ActionAlpha() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	protected void doValidation() {
		LOGGER.debug("Entering doValidation");
		for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
			String n = (String) e.nextElement();
			String[] vals = request.getParameterValues(n);
			LOGGER.debug("{}: {}", n, vals[0]);
		}

		String s = getName();
		try {
			Integer.parseInt(getAge());
		} catch (NumberFormatException e) {
			addErrorMessage(MessageUtils.XPRODUCT_ADMIN_AGE_INPUT_ERROR);
			return;
		}

		if (StringUtils.isBlank(s)) {
			addErrorMessage(MessageUtils.XPRODUCT_ADMIN_NAME_INPUT_ERROR);
			return;
		}
	}

	protected String doExecute() throws Exception {
		LOGGER.debug("Entering doExecute");
		return SUCCESS;
	}

	public String doDefault() throws Exception {
		LOGGER.debug("Entering doDefault");
		return INPUT;
	}

	public String doViewAll() throws Exception {
		LOGGER.debug("Entering doViewAll");
		return "ok";
	}
	
	private String getParameter(String name)
    {
        return this.getHttpRequest().getParameter(name);
    }

	@ActionViewDataMappings({ SUCCESS })
	public Map<String, Object> getSuccessDataMap() throws SearchException {
		MapBuilder<String, Object> builder = MapBuilder.<String, Object>newBuilder()
				.add(Config.NAME, getName())
				.add(Config.AGE, getAge())
				.add(MessageUtils.BASEURL, ComponentAccessor.getComponent(BaseUrl.class).getCanonicalBaseUrl());
		return builder.toMap();
	}

}